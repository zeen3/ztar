declare type EnDeCoder<T> = {
    encode(v: T, into: Uint8Array, offset: number, length: number): void;
    decode(from: Uint8Array, offset: number, length: number): T;
};
interface TarHeaderOffsets {
    'name': string;
    'mode': number;
    'uid': number;
    'gid': number;
    'size': number;
    'mtime': Date;
    'chksum': number;
    'type': string;
    'linkname': string;
    'ustar': string;
    'owner': string;
    'group': string;
    'devmajor': number;
    'devminor': number;
    'prefix': number;
    'padding': number;
}
declare type Offset<T extends keyof TarHeaderOffsets> = [T, number, EnDeCoder<TarHeaderOffsets[T]>];
export declare class TarHeader {
    private name;
    private mtime;
    private size;
    private mode;
    private uid;
    private gid;
    private type;
    private linkname;
    private owner;
    private group;
    private devmajor;
    private devminor;
    private prefix;
    static offsets: Offset<keyof TarHeaderOffsets>[];
    private ustar;
    private padding;
    private chksum;
    constructor(name: string, mtime?: Date, size?: number, mode?: number, uid?: number, gid?: number, type?: number, linkname?: string, owner?: string, group?: string, devmajor?: number, devminor?: number, prefix?: string);
    static calculateChecksum(buffer: Uint8Array): number;
    valueOf(): Uint8Array;
    load(from: Uint8Array): this;
    static from(value: Uint8Array): TarHeader;
    static fromFile(file: File | Promise<File>): Promise<TarHeader>;
    static fromObject(obj: any): TarHeader;
}
export {};
//# sourceMappingURL=header.d.ts.map
import { TarHeader as Header } from './header';
declare type MaybePromise<T> = T | Promise<T>;
declare type TarWriterSink = {
    header: MaybePromise<Header>;
    body?: MaybePromise<ArrayBuffer> | MaybePromise<ReadableStream> | MaybePromise<undefined | null>;
};
export declare class TarTape {
    private _ready;
    private written;
    private body;
    private closed;
    write(data: TarWriterSink): Promise<this>;
    align(): this;
    private blob?;
    getAsBlob(): Blob;
    close(): void;
    readonly ready: Promise<this>;
    appendFile(f: MaybePromise<File>): Promise<this>;
    appendArrayBuffer(name: string, f: MaybePromise<ArrayBuffer>): Promise<this>;
    createWriteStream(head: any | Header): WritableStream<any>;
    createTextStream(head: any | Header): WritableStream<string>;
}
export {};
//# sourceMappingURL=tar.d.ts.map

const txe = new TextEncoder
const txd = new TextDecoder
type EnDeCoder<T> = {
    encode(v: T, into: Uint8Array, offset: number, length: number): void;
    decode(from: Uint8Array, offset: number, length: number): T;
}
const txs: EnDeCoder<string> = {
    encode(str, into, off, len) {
        const ret = txe.encode(str)
        if (ret.length > len)
            throw new RangeError(`Maximum for this field is ${len}`)
        
        for (let i = 0; i < len; ++i) {
            into[i + off] = ret[i] | 0
        }
    },
    decode(str, off, len) {
        const s = str.subarray(off, off + len)
        let n0 = s.indexOf(0), first = n0 === -1 ? len : n0
        if (n0 !== -1)
            while (n0++ < len)
                if (str[n0] !== 0)
                    throw new TypeError('No nulls beyond first null in strings')
        return txd.decode(s.subarray(0, first))
    }
}
const nus: EnDeCoder<number> = {
    encode(num, into, off, len) {
        for (let i = len; i--;) {
            into[off + i] = 0x30 + (num & 7)
            num >>>= 3
        }
    },
    decode(num, off, len) {
        let ret = 0
        for (let i = 0; i < len; ++i) {
            const v = num[i + off]
            ret *= 8
            if (v === 0x20) continue
            ret += (v - 0x30)
        }
        return ret
    }
}
const dus: EnDeCoder<Date> = {
    encode(date, into, off, len) {
        let i = 12, val = (+date / 1_000) >>> 0
        const data = into.subarray(off, off + len).fill(0x20)
        while (val > 0) {
            data[--i] += 16 + (val & 7)
            val >>>= 3
        }
    },
    decode(date, off, len) {
        let ret = 0
        for (let i = 0; i < len; ++i) {
            ret *= 8
            const v = date[i + off]
            if (v === 0x20) continue
            ret += (v - 0x30)
        }
        return new Date(ret * 1_000)
    }
}
const chk: EnDeCoder<number> = {
    encode(v, into, off) {
        into[off + 7] = 0x20
        into[off + 6] = 0
        into[off + 5] = 0x30 + (v & 7)
        v >>>= 3
        into[off + 4] = 0x30 + (v & 7)
        v >>>= 3
        into[off + 3] = 0x30 + (v & 7)
        v >>>= 3
        into[off + 2] = 0x30 + (v & 7)
        v >>>= 3
        into[off + 1] = 0x30 + (v & 7)
        v >>>= 3
        into[off] = 0x30 + (v & 7)
    },
    decode(from, off) {
        return (from[off] << 15) |
            (from[off + 1] << 12) |
            (from[off + 2] << 9) |
            (from[off + 3] << 6) |
            (from[off + 4] << 3) |
            (from[off + 5] << 0)
    }
}

interface TarHeaderOffsets {
    'name': string;
    'mode': number;
    'uid': number;
    'gid': number;
    'size': number;
    'mtime': Date;
    'chksum': number;
    'type': string;
    'linkname': string;
    'ustar': string;
    'owner': string;
    'group': string;
    'devmajor': number;
    'devminor': number;
    'prefix': number;
    'padding': number;
};

type Offset<T extends keyof TarHeaderOffsets> = [
    T, number, EnDeCoder<TarHeaderOffsets[T]>];
export class TarHeader {
    static offsets: Offset<keyof TarHeaderOffsets>[] = [
        ['name', 100, txs],
        ['mode', 8, nus],
        ['uid', 8, nus],
        ['gid', 8, nus],
        ['size', 12, nus],
        ['mtime', 12, dus],
        ['chksum', 8, chk],
        ['type', 1, nus],
        ['linkname', 100, txs],
        ['ustar', 8, txs],
        ['owner', 32, txs],
        ['group', 32, txs],
        ['devmajor', 8, nus],
        ['devminor', 8, nus],
        ['prefix', 155, txs],
        ['padding', 12, nus]
    ]
    private ustar = 'ustar\0'
    private padding = '\0'.repeat(12)
    private chksum = 0;
    constructor(
        private name: string,
        private mtime = new Date,
        private size = 0,
        private mode = 0o777,
        private uid = 1_000,
        private gid = 1_000,
        private type = 0,
        private linkname = '',
        private owner = '',
        private group = '',
        private devmajor = 0,
        private devminor = 0,
        private prefix = ''
    ) {}
    static calculateChecksum(buffer: Uint8Array): number {
        const chk = buffer.fill(0x20, 148, 156).reduce((a, b) => a + b, 0)
        nus.encode(chk, buffer, 148, 8)
        return chk
    }
    valueOf(): Uint8Array {
        const buffer = new Uint8Array(512)
        let offset = 0
        for (const [key, length, {encode}] of TarHeader.offsets) {
            encode(this[key], buffer, offset, length)
            offset += length
        }
        TarHeader.calculateChecksum(buffer)
        return buffer
    }
    load(from: Uint8Array): this {
        let offset = 0
        for (const [key, length, {decode}] of TarHeader.offsets) {
            this[key] = decode(from, offset, length)
            offset += length
        }
        return this
    }
    static from(value: Uint8Array) {
        if (value.byteLength !== 512)
            throw new RangeError('Must be 512 byte headers')
        return new TarHeader('').load(value)
    }
    static async fromFile(file: File | Promise<File>) {
        const {name, lastModified, size} = await file
        return new TarHeader(name, new Date(lastModified), size)
    }
    static fromObject(obj: any) {
        return new TarHeader(
            obj.name,
            obj.mtime || obj.lastModified,
            obj.size || obj.byteLength || obj.length,
            obj.mode,
            obj.uid || obj.userid,
            obj.gid || obj.groupid,
            obj.type || obj.variant,
            obj.linkname || obj.linkName || obj.ln,
            obj.uname || obj.owner || obj.username || obj.user,
            obj.gname || obj.group || obj.groupname,
            obj.devmajor || obj.majDev,
            obj.devminor || obj.minDev,
            obj.prefix || obj.linkPrefix
        )
    }
}

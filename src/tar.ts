
import {TarHeader as Header} from './header'
type MaybePromise<T> = T | Promise<T>;
type TarWriterSink = {
    header: MaybePromise<Header>,
    body?: MaybePromise<ArrayBuffer> |
        MaybePromise<ReadableStream> |
        MaybePromise<undefined | null>
};

//@inline
const toArrayBuffer = (f: MaybePromise<BodyInit>) => Promise.resolve(f)
    .then(data => new Response(data).arrayBuffer());

export class TarTape {
    private _ready = Promise.resolve(this)
    private written = 0n;
    private body: (ArrayBuffer | ArrayBufferView)[] = []
    private closed = false
    async write(data: TarWriterSink) {
        if (this.closed) throw new TypeError('Cannot write to a closed tape')
        const head = await data.header
        this.body.push(head.valueOf())
        this.written += 512n
        if ('body' in data) {
            const body = await data.body
            if (body == null) return this.align()
            if (ArrayBuffer.isView(body) || body instanceof ArrayBuffer) {
                this.body.push(body)
                this.written += BigInt(body.byteLength)
            } else if (body instanceof ReadableStream) {
                const reader = body.getReader()
                while (1) {
                    const {value, done} = await reader.read()
                    if (done) return this.align()
                    this.body.push(value)
                    this.written += value.byteLength
                }
            }
        }
        return this.align()
    }
    align() {
        const diff = (this.written | 0x1ffn) - this.written
        if (diff !== 0x1ffn) {
            this.body.push(new ArrayBuffer(Number(diff) + 1025))
            this.written += diff + 1025n
        } else {
            this.body.push(new ArrayBuffer(1024))
            this.written += 1024n
        }
        return this
    }
    private blob?: Blob;
    getAsBlob() {
        if (this.blob) return this.blob
        this.close()
        const {body} = this
        delete this.body
        const blob = new Blob(body)
        this.blob = blob
        return blob
    }
    close() {
        this.closed = true
    }
    get ready() {
        const r = this._ready.then(
            self => self.closed ?
            Promise.reject(new TypeError('Cannot write to a closed tape')) :
            Promise.resolve(self)
        );
        this._ready = r
        return r
    }
    async appendFile(f: MaybePromise<File>): Promise<this> {
        await this.ready
        this._ready = this.write({
            header: Header.fromFile(f),
            body: toArrayBuffer(f)
        })
        return this._ready
    }
    async appendArrayBuffer(name: string, f: MaybePromise<ArrayBuffer>) {
        await this.ready
        this._ready = this.write({
            header: Promise.resolve(f).then(f => new Header(name, new Date(), f.byteLength)),
            body: f
        })
        return this._ready
    }
    createWriteStream(head: any | Header) {
        const header = Header.fromObject(head)
        const {readable, writable} = new TransformStream
        this._ready = this.ready.then(() => this.write({
            header,
            body: readable
        }))
        
        return writable
    }
    createTextStream(head: any | Header) {
        const header = Header.fromObject(head)
        const tx = new TextEncoder
        const {readable, writable} = new TransformStream({
            transform(ch: string, ct: TransformStreamDefaultController<Uint8Array>) {
                ct.enqueue(tx.encode(ch))
            }
        })
        this._ready = this.ready.then(() => this.write({
            header, body: readable
        }))
        return writable
    }
}